;;; init-looknfeel.el --- Look'n'feel

;; Time-stamp: <2017-01-04 11:27:42>
;; Copyright (C) 2016 Pierre Lecocq

;;; Commentary:

;;; Code:

;; Colors

;; (use-package darkmine-theme :ensure t)
;; (use-package tao-theme :ensure t)
;; (use-package gruvbox-theme :ensure t)

(use-package doom-themes :ensure t)


;; Global settings (defaults)
(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
      doom-themes-enable-italic t) ; if nil, italics is universally disabled

;; Load the theme (doom-one, doom-molokai, etc); keep in mind that each theme
;; may have their own settings.
(load-theme 'doom-one t)

;; Enable flashing mode-line on errors
(doom-themes-visual-bell-config)

;; Enable custom neotree theme
;; (doom-themes-neotree-config)  ; all-the-icons fonts must be installed!

;; Corrects (and improves) org-mode's native fontification.
(doom-themes-org-config)

;; (when (display-graphic-p)
;;   (require 'color)
;;   (set-face-attribute 'fringe nil :background "grey13")
;;   (set-fringe-mode 10)
;;   (set-face-attribute 'mode-line nil :box nil)
;;   (set-face-attribute 'mode-line-inactive nil :box nil)
;;   (let ((bg (face-attribute 'default :background))
;;         (fg (face-attribute 'default :foreground)))
;;     (custom-set-faces
;;      `(font-lock-string-face ((t (:foreground "IndianRed"))))
;;      `(vertical-border ((t (:foreground ,(color-lighten-name bg 15)))))
;;      `(isearch ((t (:background "DodgerBlue" :foreground "white"))))
;;      `(which-func ((t (:inherit mode-line))))
;;      `(idle-highlight ((t (:inherit isearch))))
;;      `(ido-subdir ((t (:inherit font-lock-function-name-face))))
;;      `(ido-first-match ((t (:inherit font-lock-variable-name-face))))
;;      `(ido-only-match ((t (:inherit font-lock-variable-name-face))))
;;      `(company-tooltip ((t (:inherit default :background ,(color-lighten-name bg 2)))))
;;      `(company-scrollbar-bg ((t (:background ,(color-lighten-name bg 10)))))
;;      `(company-scrollbar-fg ((t (:background ,(color-lighten-name bg 5)))))
;;      `(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
;;      `(company-tooltip-common ((t (:inherit font-lock-constant-face)))))))

;; Transparency

(defun pl-transparency (value)
  "Set the transparency of the frame window.
Argument VALUE 0 is transparent, 100 is opaque."
  (interactive "nTransparency Value (0 - 100): ")
  (when (display-graphic-p)
    (set-frame-parameter (selected-frame) 'alpha value)))

;; Packages

(use-package abbrev
  :diminish abbrev-mode)

(use-package autopair :ensure t
  :config (autopair-global-mode t)
  :diminish autopair-mode)


(use-package idle-highlight-mode :ensure t
  :diminish idle-highlight-mode)

(use-package rainbow-delimiters :ensure t)

(use-package beacon :ensure t)
(beacon-mode 1)

(provide 'init-looknfeel)

;;; init-looknfeel.el ends here
