;;; init-neotree.el --- 

;; Time-stamp: <>
;; Copyright (C) 2017 Pierre Lecocq

;;; Commentary:

;;; Code:

(use-package neotree :ensure t)

(provide 'init-neotree)


;;; init-neotree.el ends here
