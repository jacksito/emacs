;;; init-yasnippet.el --- 

;; Time-stamp: <>
;; Copyright (C) 2017 Jack Kourie

;;; Commentary:

;;; Code:


(use-package yasnippet :ensure t
  :init (progn
          (yas-global-mode 1)
          ))
(provide 'init-yasnippet)


;;; init-yasnippet.el ends here
