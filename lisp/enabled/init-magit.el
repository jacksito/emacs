;;; init-magit.el --- 

;; Time-stamp: <>
;; Copyright (C) 2017 Jack Kourie

;;; Commentary:

;;; Code:

(use-package magit :ensure t)

(provide 'init-magit)
;;; init-magit.el ends here
