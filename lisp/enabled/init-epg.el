;;; init-epg.el --- EasyPG

;; Time-stamp: <2017-05-15 09:08:34>
;; Copyright (C) 2017 Pierre Lecocq

;;; Commentary:

;;; Code:

(require 'epa-file)

(epa-file-enable)

(provide 'init-epg)

;;; init-epg.el ends here
