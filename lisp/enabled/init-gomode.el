;;; init-gomode.el --- 

;; Time-stamp: <>
;; Copyright (C) 2017 Jack Kourie

;;; Commentary:

;;; Code:

(use-package go-mode :ensure t)
(use-package go-autocomplete :ensure t)

(add-hook 'before-save-hook 'gofmt-before-save)
(add-hook 'go-mode-hook (lambda ()
                          (local-set-key (kbd "M-.") 'godef-jump)))

(provide 'init-gomode)
;;; init-gomode.el ends here
