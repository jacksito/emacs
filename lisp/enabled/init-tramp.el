;;; init-tramp.el --- 

;; Time-stamp: <>
;; Copyright (C) 2017 Jack Kourie

;;; Commentary:

;;; Code:

(use-package tramp :ensure t)

(setq tramp-default-method "ssh")

(provide 'init-tramp)

;;; init-tramp.el ends here
