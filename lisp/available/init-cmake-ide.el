;;; init-cmake-ide.el --- 

;; Time-stamp: <>
;; Copyright (C) 2017 Jack Kourie

;;; Commentary:

;;; Code:

(use-package rtags :ensure t)
(use-package cmake-ide :ensure t)
;; (cmake-ide-setup)
;; (setq cmake-ide-build-pool-dir "/home/jafu/cmake_builds/")
;; (setq cmake-compile-command "buildutil/build.sh clean")
;; (setq cmake-ide-build-pool-use-persistent-naming 1)
;; (set 'cmake-ide-build-dir "/home/jafu/nbackend/")



(provide 'init-cmake-ide)

;;; init-cmake-ide.el ends here
