;;; init-nyan-mode.el --- 

;; Time-stamp: <>
;; Copyright (C) 2017 Jack Kourie

;;; Commentary:

;;; Code:

(use-package nyan-mode :ensure t)

(when (display-graphic-p)
	(nyan-mode 1)
        (nyan-start-animation)
	)

(provide 'init-nyan-mode)

;;; init-nyan-mode.el ends here
