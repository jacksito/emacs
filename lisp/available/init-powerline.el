;;; init-powerline.el --- 

;; Time-stamp: <>
;; Copyright (C) 2017 Jack Kourie

;;; Commentary:

;;; Code:

(use-package powerline :ensure t)
(when (display-graphic-p)
  (
   (powerline-default-theme)
   )

(provide 'init-powerline)
;;; init-powerline.el ends here
